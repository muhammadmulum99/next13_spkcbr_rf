const express = require("express");
const cors = require("cors");

const app = express();
const db = require("./app/models");
const Role = db.role;
const Penyakit = db.penyakit;
const faktor = db.faktor;
const Pengetahuan = db.pengetahuan;


// db.sequelize.sync().then(() => {
//     console.log('Drop and Resync Db');
//     // initial();
//   });

db.sequelize.sync({ force: true }).then(() => {
  console.log('Drop and Resync Db');
  initial();
});

var corsOptions = {
  origin: "http://localhost:3000"
};

app.use(cors(corsOptions));

function initial() {

  Pengetahuan.create({
    id: 1,
    id_faktor: [1, 2],
    diagnosisId: 1,
    diagnosa: "FLu",
    BobotKepastian: [0.5, 0.6],
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });


  Pengetahuan.create({
    id: 2,
    id_faktor: [2],
    diagnosisId: 2,
    diagnosa: "Bronkitis",
    BobotKepastian: [0.6],
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });

  Role.create({
    id: 1,
    name: "admin"
  });

  Role.create({
    id: 2,
    name: "user"
  });

  faktor.create({
    id: 1,
    faktor_penyebab: "Sakit A",
    bobot_faktor: 0.5

  })
  faktor.create({
    id: 2,
    faktor_penyebab: "Sakit B",
    bobot_faktor: 0.6

  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });

  Penyakit.create({
    nama_penyakit: "Flu",
    detail_penyakit: "Flu disebabkan oleh virus influenza yang menginfeksi hidung, tenggorokan, dan paru-paru. Virus penyebab gangguan respirasi ini dapat menyebar melalui udara, benda yang telah terkontaminasi, maupun kontak fisik dengan penderita flu.",
    solusi_penyakit: "Cuci tangan secara rutin, untuk menghindari penyebaran virus flu ke benda atau orang lain,  Gunakan tisu untuk menutup mulut dan hidung ketika batuk atau bersin, lalu buang tisu tersebut, Pastikan tubuh terhidrasi dengan minum banyak cairan, seperti air putih, jus buah, atau sup hangat, guna mencegah dehidrasi,  Konsumsi obat pereda nyeri seperti paracetamol, untuk meredakan gejala demam dan nyeri otot."
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });


  Penyakit.create({
    nama_penyakit: "Faringitis",
    detail_penyakit: "Faringitis adalah peradangan pada tenggorokan atau faring. Keluhan ini disebabkan oleh infeksi bakteri maupun virus. Faringitis dapat ditangani bedasarkan penyebabnya. Misalnya, faringitis yang disebabkan oleh bakteri dapat diobati menggunakan antibiotik.",
    solusi_penyakit: "Perawatan secara mandiri berikut dapat membantu mengatasi ketidaknyamanan jika sedang mengidap faringitis. Beberapa hal yang dapat dilakukan, yaitu: Jangan merokok, karena hal ini akan memperparah kondisi faringitis, Hindari memakan makanan yang pedas, panas, dan berminyak, Minum cairan lebih banyak untuk melegakan tenggorokan, Perbanyak minum minuman hangat."
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });

  Penyakit.create({
    nama_penyakit: "Laringitis",
    detail_penyakit: "Gangguan respirasi lainnya adalah laringitis, yaitu peradangan yang terjadi pada laring atau pita suara. Keluhan ini umumnya disebabkan oleh penggunaan laring yang berlebihan, iritasi, atau infeksi.",
    solusi_penyakit: "laringitis bisa diatasi dengan mengistirahatkan pita suara, yaitu dengan mengurangi berbicara. Menjaga tenggorokan tetap lembab dengan minum banyak cairan dan menjaga kelembaban udara dengan memasang humidifier juga bisa membantu laringitis cepat sembuh."
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });

  Penyakit.create({
    nama_penyakit: "Asma",
    detail_penyakit: "Asma merupakan gangguan respirasi yang ditandai dengan peradangan pada saluran pernapasan. Keluhan ini membuat saluran napas mengalami penyempitan. Penyebabnya bisa karena alergi, paparan asap, polusi, hingga udara dingin.",
    solusi_penyakit: "Ada dua hal yang perlu dilakukan dalam pengobatan asma, yakni meredakan gejala dan mencegah gejala kambuh.  Oleh karena itu, pengidap asma perlu disiplin menjalani pengobatan dengan dokter agar asma tetap terkendali.  Di samping melakukan pengobatan, pengidap asma juga harus menghindari dari hal-hal yang memicu kekambuhan.   Biasanya, dokter merekomendasikan inhaler sebagai pengobatan saat gejala asma muncul.  Namun, penggunaan inhaler juga berpotensi menyebabkan efek samping bagi pengguna.  Apabila terjadi serangan asma dengan gejala yang semakin parah, meskipun sudah melakukan penanganan dengan inhaler maupun obat, maka perlu tindakan medis di rumah sakit.  Pasalnya, asma juga dapat membahayakan nyawa pengidapnya"
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });

  Penyakit.create({
    nama_penyakit: "Bronkitis",
    detail_penyakit: "Bronkitis terjadi ketika saluran yang membawa udara ke paru-paru atau bronkus mengalami peradangan. Akibatnya, gangguan respirasi ini menyebabkan penderitanya batuk berdahak. Bronkitis dapat terjadi akut atau kronis. Selain batuk berdahak, gejala yang menyertai bronkitis adalah dada sesak, dahak berwarna kuning atau hijau, hingga demam.",
    solusi_penyakit: "Pengidap juga bisa melakukan penanganan secara mandiri untuk membantu meringankan gejalanya, seperti berikut ini. Cukup istirahat. Minum air putih lebih banyak. Menghirup uap dari air hangat untuk membantu mengencerkan lendir  pada saluran napas. Tidak merokok.  Pakai masker ketika harus beraktivitas di luar rumah untuk mencegah paparan zat kimia. "
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });

  Penyakit.create({
    nama_penyakit: "Emfisme",
    detail_penyakit: "Emfisema adalah penyakit kronis atau jangka panjang akibat kerusakan pada alveolus, yaitu kantong udara kecil pada paru-paru. Gangguan respirasi ini lebih sering dialami oleh perokok aktif. Penderita emfisema dapat mengalami gejala batuk kronis dan sesak napas, bahkan saat berolahraga ringan atau menaiki tangga.",
    solusi_penyakit: "Bagi pengidap emfisema yang merokok, langkah awal pengobatan adalah dengan berhenti merokok untuk menghentikan efek kerusakan akibat emfisema.   Karena emfisema tidak dapat disembuhkan, maka penanganan dilakukan untuk meringankan gejala yang dirasakan pengidapnya, serta memperlambat perkembangan penyakit. Penanganan emfisema ada beberapa jenis, yaitu: Obat-obatan, Terapi & Pembedahan."
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });

  Penyakit.create({
    nama_penyakit: "Pneumonia",
    detail_penyakit: "Pneumonia adalah gangguan respirasi pada paru-paru yang disebabkan oleh infeksi virus, bakteri, atau jamur. Pneumonia juga bisa disebabkan oleh virus SARS-CoV-2 yang menyebabkan COVID-19. Gejala pneumonia cukup bervariasi. Namun, pneumonia umumnya ditandai dengan gejala, seperti batuk, demam, sesak napas, dan menggigil.",
    solusi_penyakit: "Pada kasus pneumonia yang masih tergolong ringan, pengidap tidak perlu dirawat di rumah sakit. Pengobatan bisa dilakukan sendiri di rumah dengan mengonsumsi antibiotik yang diresepkan oleh dokter serta banyak beristirahat dan minum."
  }).then(createdData => {
    console.log("Data berhasil ditambahkan:", createdData);
  })
    .catch(error => {
      console.error("Gagal menambahkan data:", error);
    });
}

// Pengetahuan.create({
//   id_faktor: [1],
//   diagnosisId: 2,
//   diagnosa: "Bronkitis",
//   BobotKepastian:[0.5],
// })

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/penyakit.routes')(app);
require('./app/routes/perhitungan.routes')(app);
require('./app/routes/faktor.routes')(app);
require('./app/routes/pengetahuan.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});