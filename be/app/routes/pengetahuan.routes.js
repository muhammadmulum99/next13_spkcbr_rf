const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");
const pengetahuan = require("../controllers/pengetahuan.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/be/pengetahuan",pengetahuan.findAll);
  app.get("/api/be/pengetahuanQuery",pengetahuan.findAllQuery);

  app.post("/api/be/pengetahuan",pengetahuan.create);

  app.get("/api/be/pengetahuan/:id",pengetahuan.findOne);

  app.put("/api/be/pengetahuan/:id",pengetahuan.update);

  app.delete("/api/be/pengetahuan/:id",pengetahuan.delete)

  // app.delete("/api/be/pengetahuan/:id",[authJwt.verifyToken],pengetahuan.delete)



  //app.get("/api/be/pengetahuan",pengetahuan.findAll);

  //app.get("/api/be/pengetahuan", controller.findAll);

//   app.get(
//     "/api/test/pengetahuan",
//     [authJwt.verifyToken],
//     controller.userBoard
//   );

};