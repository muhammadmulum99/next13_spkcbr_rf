const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");
const penyakit = require("../controllers/penyakit.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/be/penyakit",penyakit.findAll);

  app.post("/api/be/penyakit",penyakit.create);

  app.get("/api/be/penyakit/:id",penyakit.findOne);

  app.put("/api/be/penyakit/:id",penyakit.update);

  app.delete("/api/be/penyakit/:id",penyakit.delete)

  // app.delete("/api/be/penyakit/:id",[authJwt.verifyToken],penyakit.delete)



  //app.get("/api/be/penyakit",penyakit.findAll);

  //app.get("/api/be/penyakit", controller.findAll);

//   app.get(
//     "/api/test/penyakit",
//     [authJwt.verifyToken],
//     controller.userBoard
//   );

};