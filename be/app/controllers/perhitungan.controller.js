const { calculateCF } = require('../spk/cffix');
const { hitungHasilCBR } = require('../spk/cbrfix');
const db = require("../models");
const Penyakit = db.penyakit;
const Pengetahuan = db.pengetahuan;

exports.hitungfix = async (req, res) => {
  try {
    // Validate request
    const inputSymptoms = req.body.symptoms;

    // const convert=FconvertSymptomsData(inputSymptoms)

    const convert = inputSymptoms.map(symptom => ({
      penyakit: String(symptom.penyakit), // Mengonversi penyakit menjadi string
      gejala: symptom.gejala,
      bobotKepastian: symptom.bobotKepastian
  }));

   
   

    // Mengambil data pengetahuan dari database
    const databasePengetahuan = await DatabasePengetahuan();

    
    if (!databasePengetahuan) {
      throw new Error("Data Pengetahuan tidak ditemukan.");
    }

    console.log("Pengetahuan^",databasePengetahuan);
    console.log(inputSymptoms)
    console.log("dfghj",convert);

    // Diagnosa menggunakan metode CBR
    const DiagnosisaCBR = hitungHasilCBR(convert, databasePengetahuan);
    const DiagnosisaCF = calculateCF(databasePengetahuan, convert);

    console.log('DiagnosisaCBR:', DiagnosisaCBR);
    console.log('DiagnosisaCF:', DiagnosisaCF);

    // Mendapatkan data penyakit berdasarkan id
    const Penyakitdata = await Penyakit.findByPk(DiagnosisaCBR[0].penyakit);

    if (!Penyakitdata) {
      throw new Error("Data penyakit tidak ditemukan.");
    }

    // Membuat objek hasil akhir
    const hasilAkhir = [{
      namaPenyakit: Penyakitdata.nama_penyakit,
      detailPenyakit: Penyakitdata.detail_penyakit,
      Solusi: Penyakitdata.solusi_penyakit
    }];

    console.log("datapengetahuan",Penyakitdata);

    // Mengirim respons
    res.json({
      data: {
        cbrDiagnosis: DiagnosisaCBR ? DiagnosisaCBR : 'Tidak ada diagnosa (CBR)',
        cfDiagnosis: DiagnosisaCF ? DiagnosisaCF : 'Tidak ada diagnosa (CF)',
        hasilAkhir: hasilAkhir
      }
    });
  } catch (error) {
    console.error("Error:", error.message);
    res.status(500).json({ message: "Terjadi kesalahan dalam memproses data penyakit."});
  }
};

// Fungsi untuk mendapatkan data pengetahuan dari database
async function DatabasePengetahuan() {
  try {

    const Datacase = await Pengetahuan.findAll();
    console.log(Datacase);
    return Datacase.map(datacase => ({
      penyakit: datacase.diagnosisId,//string ex "1"
      gejala: JSON.parse(datacase.id_faktor),//json ex [1,2]
      bobotKepastian: datacase.BobotKepastian.slice(1, -1)
      .split(",")
      .map(val => parseFloat(val.trim()))
    }));

  } catch (error) {
    throw error;
  }
}


const FconvertSymptomsData = (symptoms) => {
  return symptoms.map(symptom => {
      return {
          penyakit: String(symptom.penyakit), // Mengonversi penyakit menjadi string
          gejala: symptom.gejala, // Mengonversi gejala menjadi string
          bobotKepastian: symptom.bobotKepastian // Mengonversi bobot kepastian menjadi string
      };
  });
};
