const db = require("../models");
const Faktor = db.faktor;
const Op = db.Sequelize.Op;

// Create and Save a new Faktor
exports.create = (req, res) => {
    // Validate request
    if (!req.body.faktor_penyebab || !req.body.bobot_faktor) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    const foto= "foto.png"
    // Create a Faktor
    const Faktors = {
      faktor_penyebab: req.body.faktor_penyebab,
      bobot_faktor: req.body.bobot_faktor,
  
    };
  
    // Save Faktor in the database
    Faktor.create(Faktors)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Faktor."
        });
      });
  };

// Retrieve all Faktors from the database.
exports.findAll = (req, res) => {
    const nama_faktor = req.query.faktor_penyebab;
    var condition = nama_faktor ? { faktor_penyebab: { [Op.like]: `%${nama_faktor}%` } } : null;
  
    Faktor.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Faktors."
        });
      });
  };

// Find a single Faktor with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Faktor.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Faktor with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Faktor with id=" + id
        });
      });
  };

// Update a Faktor by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    Faktor.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Faktor was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Faktor with id=${id}. Maybe Faktor was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Faktor with id=" + id
        });
      });
  };

// Delete a Faktor with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Faktor.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Faktor was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Faktor with id=${id}. Maybe Faktor was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Faktor with id=" + id
        });
      });
  };

// Delete all Faktors from the database.
// exports.deleteAll = (req, res) => {
//     Faktor.destroy({
//       where: {},
//       truncate: false
//     })
//       .then(nums => {
//         res.send({ message: `${nums} Faktors were deleted successfully!` });
//       })
//       .catch(err => {
//         res.status(500).send({
//           message:
//             err.message || "Some error occurred while removing all Faktors."
//         });
//       });
//   };

// Find all published Faktors
// exports.findAllPublished = (req, res) => {
//     Faktor.findAll({ where: { published: true } })
//       .then(data => {
//         res.send(data);
//       })
//       .catch(err => {
//         res.status(500).send({
//           message:
//             err.message || "Some error occurred while retrieving Faktors."
//         });
//       });
//   };