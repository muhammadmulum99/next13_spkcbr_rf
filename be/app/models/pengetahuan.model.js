const Faktor=require('./faktor.model')

module.exports = (sequelize, Sequelize) => {
    const Pengetahuan = sequelize.define("pengetahuan", {
      id_faktor: {
        type: Sequelize.JSON,
        defaultValue: [],  
      },
      diagnosisId: {
        type: Sequelize.STRING
      },
      diagnosa: {
        type: Sequelize.STRING
      },
      BobotKepastian:{
        type: Sequelize.JSON,
        defaultValue: [], 
      },
    });  
    return Pengetahuan;
  };