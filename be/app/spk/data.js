const faktor=require("../controllers/faktor.controller")
const pengetahuan= require("../controllers/pengetahuan.controller")


const symptomsData = [
    { id: 1, name: 'demam' },
    { id: 2, name: 'menggigil' },
    { id: 3, name: 'nyeri otot' },
    { id: 4, name: 'batuk' },
    { id: 5, name: 'pilek' },
    { id: 6, name: 'sakit kepala' },
    { id: 7, name: 'kelelahan' },
    { id: 8, name: 'nyeri / sakit tenggorokan' },
    { id: 9, name: 'gatal ditenggorokan' },
    { id: 10, name: 'sulit menelan' },
    { id: 11, name: 'demam' },
    { id: 12, name: 'pegel lini' },
    { id: 13, name: 'mual muntah' },
    { id: 14, name: 'kelenjar getah bening dileher membengkak' },
    { id: 15, name: 'batuk terus menerus' },
    { id: 16, name: 'terdapat dahak ditenggorokan' },
    { id: 17, name: 'kesulitan menelan' },
    { id: 18, name: 'tenggorokan terasa ada benjolan' },
    { id: 19, name: 'sakit tenggorokan' },
    { id: 20, name: 'suara serak / bahkan hilang' },
    { id: 21, name: 'sesak napas' },
    { id: 22, name: 'napas bunyi' },
    { id: 23, name: 'dada terasa berat' },
  ];
  
  const casesData = [
    { symptomIds: [1, 2, 3,4,5], diagnosis: 'Flu', positiveFactor: 0.7, negativeFactor: 0.3 },
    { symptomIds: [8,9,10,11,12,13, 14], diagnosis: 'Faringitis', positiveFactor: 0.5, negativeFactor: 0.5 },
    { symptomIds: [15,16,17,18,19,20], diagnosis: 'Laringitis', positiveFactor: 0.8, negativeFactor: 0.2 },
    { symptomIds: [4,21,22,23], diagnosis: 'Asma', positiveFactor: 0.6, negativeFactor: 0.4 },
    // ... tambahkan lebih banyak kasus di sini
  ];

  // const casesData = async()=>{
  //   try {
  //     const Datacase= await pengetahuan.findAll();
  //     console.log(Datacase)
  //     // return Datacase.map(datacase => datacase.get({plain:true}))
  //     return Datacase.map(datacase => ({
  //       symptomIds: datacase.id_faktor.split(',').map(id => parseInt(id)), // Jika simpanan symptomIds dalam format string yang dipisahkan koma
  //       diagnosis: datacase.diagnosis,
  //       positiveFactor: datacase.positiveFactor,
  //       negativeFactor: datacase.negativeFactor
  //     }))
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  // const symptomsData = async ()=>{
  //   try {
  //     const symptoms = await faktor.findAll();
  //     console.log(symptoms)
  //     return symptoms.map(symptom => symptom.get({ plain: true }));
  //   } catch (error) {
  //     throw error;
  //   }
  // }
  
  module.exports = {
    symptomsData,
    casesData
  };
  