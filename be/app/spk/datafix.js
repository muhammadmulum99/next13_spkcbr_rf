const DatabasePengetahuan = [
    { penyakit: 'Flu', gejala: ['Demam', 'Batuk', 'Pilek'], bobotKepastian: [0.8, 0.4, 0.2] },
    { penyakit: 'Pilek Biasa', gejala: ['Pilek', 'Sakit','Tenggorokan'], bobotKepastian: [0.5, 0.2, 0.8] },
    { penyakit: 'Bronkitis', gejala: ['Batuk Kronis', 'Nyeri ','Dada'], bobotKepastian: [0.3, 0.2, 0.7] },
];

module.exports = {
    DatabasePengetahuan
  };
  