const { DatabasePengetahuan } = require('./datafix');

const database = DatabasePengetahuan;


function hitungHasilCBR(input, db) {
    let maxHasil = 0; // Variabel untuk menyimpan hasil tertinggi
    let penyakitMaxHasil = null; // Variabel untuk menyimpan penyakit dengan hasil tertinggi

    db.forEach((penyakitDb, indexDb) => {
        let totalBobotDb = penyakitDb.bobotKepastian.reduce((acc, curr) => acc + curr, 0);
        let hasilPenyakit = 0;

        input.forEach((penyakitInput, indexInput) => {
            if (penyakitDb.penyakit === penyakitInput.penyakit) {
                penyakitDb.bobotKepastian.forEach((bobotDb, idxDb) => {
                    hasilPenyakit += bobotDb * penyakitInput.bobotKepastian[idxDb];
                });
            }
        });

        let hasilPersentase = (hasilPenyakit / totalBobotDb) * 100;

        if (hasilPersentase > maxHasil) { // Jika hasilPersentase lebih besar dari maxHasil, update maxHasil dan penyakitMaxHasil
            maxHasil = hasilPersentase;
            penyakitMaxHasil = penyakitDb;
        }
    });

    if (penyakitMaxHasil) { // Jika ada penyakit dengan hasil tertinggi
        return [{ penyakit: penyakitMaxHasil.penyakit, hasil: maxHasil }];
    } else { // Jika tidak ada penyakit yang cocok
        return [];
    }
}


// const hasilPerhitungan = hitungHasilCBR(inputan, database);
// console.log(hasilPerhitungan);

module.exports = {
    hitungHasilCBR
};

