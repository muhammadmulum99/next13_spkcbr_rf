// Contoh database kasus penyakit dan gejalanya
const database = [
    { penyakit: 'Flu', gejala: ['Demam', 'Batuk', 'Pilek'] },
    { penyakit: 'Pilek Biasa', gejala: ['Pilek', 'Sakit Tenggorokan'] },
    { penyakit: 'Bronkitis', gejala: ['Batuk Kronis', 'Nyeri Dada'] },
    // Tambahkan lebih banyak kasus penyakit dan gejalanya di sini
  ];
  
  // Fungsi untuk mencocokkan gejala dengan kasus-kasus dalam database
  function matchSymptoms(symptoms) {
    let matchedCases = [];
    database.forEach((caseData) => {
      let matchCount = 0;
      caseData.gejala.forEach((gejala) => {
        if (symptoms.includes(gejala)) {
          matchCount++;
        }
      });
      // Hitung tingkat kesesuaian (match ratio)
      const matchRatio = matchCount / caseData.gejala.length;
      matchedCases.push({ penyakit: caseData.penyakit, matchRatio: matchRatio });
    });
    return matchedCases;
  }
  
  // Fungsi untuk menghitung CF berdasarkan tingkat kesesuaian (match ratio)
  function calculateCF(matchRatio) {
    // Misalkan, kita menggunakan fungsi sigmoid untuk mengonversi match ratio menjadi nilai CF
    const CF = 2 / (1 + Math.exp(-matchRatio)) - 1;
    return CF;
  }
  
  // Fungsi utama untuk melakukan diagnosa
  function diagnose(symptoms) {
    const matchedCases = matchSymptoms(symptoms);
    let maxCF = -1;
    let diagnosis = '';
  
    matchedCases.forEach((caseData) => {
      const CF = calculateCF(caseData.matchRatio);
      if (CF > maxCF) {
        maxCF = CF;
        diagnosis = caseData.penyakit;
      }
    });
  
    return { diagnosis: diagnosis, CF: maxCF };
  }
  
  // Contoh penggunaan
  const patientSymptoms = ['Demam', 'Batuk'];
  const result = diagnose(patientSymptoms);
  console.log('Diagnosis:', result.diagnosis);
  console.log('CF:', result.CF);
  

  