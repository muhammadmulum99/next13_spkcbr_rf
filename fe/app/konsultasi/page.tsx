import React from 'react'
import AddKonsultasi from '../components/konsultasidata/AddKonsultasi'
import Header from '../components/template/Header'
export default function Konsultasi() {
    return (
        <div>
            <Header />
            <main className="max-w-4xl mx-auto mt-4">
                <div className=" my-5 flex flex-col gap-4">
                    <h1 className="text-2xl font-bold"></h1>
                    <AddKonsultasi />

                </div>
                {/* <PenyakitList penyakits={penyakits}/> */}
            </main>
            {/* <AddKonsultasi /> */}

        </div>
    )
}
