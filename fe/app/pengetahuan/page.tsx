import React from 'react'

import { getAllPengetahuan } from '@/api';
import PengetahuanList from '../components/pengetahuan/PengetahuanList';
import AddPengetahuan from '../components/pengetahuan/AddPengetahuan';
import Header from '../components/template/Header';

export default async function Pengetahuan() {
    const pengetahuans=await getAllPengetahuan();
    console.log(pengetahuans);
  return (
    <div>
    <Header/>
    <main className="max-w-4xl mx-auto mt-4">
    <div className="text-center my-5 flex flex-col gap-4">
      <h1 className="text-2xl font-bold">List Pengetahuan</h1>
      <AddPengetahuan/>
      
    </div>
    <PengetahuanList pengetahuans={pengetahuans}/>
  </main>
  </div>
  )
}
