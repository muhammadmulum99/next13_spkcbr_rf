'use client'
import React, { FormEventHandler, useState, useEffect } from 'react'
// import Modal from './Modal'
import { addPenyakit } from '@/api'
import { IPenyakit } from '@/types/penyakit'
import { AiFillPlusCircle } from 'react-icons/ai'
import { useRouter } from 'next/navigation'
import Swal from 'sweetalert2'
import axios from 'axios'
import Modal from './Modal'
import Header from '../template/Header'
import { Inder } from 'next/font/google'

interface FaktorPenyebab {
    id: number;
    faktor_penyebab: string;
}

interface FaktorPenyakit {
    id: number;
    nama_penyakit: string;
    detail_penyakit: string;
    solusi: string;
    dataDariBackend:any[]
}

interface Diagnosis {
    penyakit: string;
    hasil?: number;
    CF?: number;
}

interface Hasil {
    namaPenyakit: string;
    detailPenyakit: string;
    Solusi: string
}

interface DiagnosaResult {
    data: {
        cbrDiagnosis: Diagnosis[];
        cfDiagnosis: Diagnosis[];
        hasilAkhir: Hasil[];
    };
}

interface SendData {
    symptoms: {
        penyakit: string;
        gejala: number[];
        bobotKepastian: number[];
    }[];
}


const AddKonsultasi = () => {
    const router = useRouter();
    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [formOpen, setFormOpen] = useState<boolean>(true);
    const [newNama, setNewNama] = useState('');
    const [newNamaBelakang, setNewNamaBelakang] = useState('');
    const [newUmur, setNewUmur] = useState('');
    const [newAlamat, setNewAlamat] = useState('');
    const [newJenisKelamin, setNewJenisKelamin] = useState('');
    const [newNoTelp, setNewNoTelp] = useState('');
    const [dataDariBackend, setDataDariBackend] = useState<FaktorPenyebab[]>([]);
    const [dataDariBackendPenyakit, setDataDariBackendPenyakit] = useState<FaktorPenyakit[]>([]);
    // const [selectedOptions, setSelectedOptions] = useState<FaktorPenyebab[]>([]);
    const [factorWeights, setFactorWeights] = useState<{ [key: number]: string }>({});
    const [diagnosaResult, setDiagnosaResult] = useState<DiagnosaResult | null>(null);
    const [sendData, setSendData] = useState<SendData>({ symptoms: [] });
    const [selectedOptions, setSelectedOptions] = useState<any[]>([]); // Tipe data any[] digunakan sementara untuk selectedOptions

    useEffect(() => {
        axios.get('http://localhost:8080/api/be/faktor')
            .then(response => {
                setDataDariBackend(response.data);
                console.log(response.data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }, []);

    useEffect(() => {
        axios.get('http://localhost:8080/api/be/penyakit')
            .then(response => {
                setDataDariBackendPenyakit(response.data);
                console.log(response.data);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }, []);


    const handleSubmitNewKonsultasi: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();

        Swal.fire(
            'Success',
            'Success Add Data',
            'success'
        )
        setFormOpen(false)
    }

    const handleDropdownChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const selectedFactorId = e.target.value;
        const selectedFactor = dataDariBackend.find((item) => item.id === parseInt(selectedFactorId));

        if (selectedFactor) {
            setSelectedOptions([...selectedOptions, selectedFactor]);
        }
    }

    

        // const [selectedOptions, setSelectedOptions] = useState<any[]>([]); // Tipe data any[] digunakan sementara untuk selectedOptions
    
        const handleSelectChange = (indexPenyakit: number, indexGejala: number, event: React.ChangeEvent<HTMLSelectElement>) => {
            const newSelectedOptions = [...selectedOptions];
            if (!newSelectedOptions[indexPenyakit]) {
                newSelectedOptions[indexPenyakit] = {
                    penyakit: dataDariBackendPenyakit[indexPenyakit].id,
                    gejala: [],
                    bobotKepastian: []
                };
            }
            newSelectedOptions[indexPenyakit].gejala[indexGejala] = dataDariBackend[indexGejala].id;
            newSelectedOptions[indexPenyakit].bobotKepastian[indexGejala] =parseInt(event.target.value);
            setSelectedOptions(newSelectedOptions);

            console.log(newSelectedOptions)
        };

    
    



    const handleWeightChange = (factorId: number, weightValue: string) => {
        setFactorWeights({ ...factorWeights, [factorId]: weightValue });
    };
    const handleSubmit: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        console.log('Gejala yang dipilih:', selectedOptions);
        console.log('Bobot faktor:', factorWeights);
        // Lakukan pengiriman data ke server atau jalankan logika diagnosis di sini
        const cases = {
            idfaktor: selectedOptions.map(option => option.id),
            nilaibobot: factorWeights
        };
        const symptomsData = selectedOptions.map(option => {
            return { id: option.id, name: option.faktor_penyebab };
        });
        const diagnosisData = {
            symptoms: symptomsData,
        };
        const datacases = {
            symptoms: [
                {
                    "penyakit": "1",
                    "gejala": [1, 2],
                    "bobotKepastian": [1, 0]
                }
            ]
        }
        const dataSendCBRandCF = {
            // datadiri: formData,
            symptoms:selectedOptions,
        };

        console.log(dataSendCBRandCF)
        axios.post('http://localhost:8080/api/be/diagnosafix', dataSendCBRandCF)
            .then(function (response) {
                console.log(response);
                console.log(diagnosisData)
                console.log(datacases)
                setDiagnosaResult(response.data);
                setModalOpen(true);
            })
            .catch(function (error) {
                console.error(error);
            });
    };

    return (
        <div>
            <div className="isolate bg-white px-6 py-24 sm:py-32 lg:px-8">
                <div
                    className="absolute inset-x-0 top-[-10rem] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[-20rem]"
                    aria-hidden="true"
                >
                    <div
                        className="relative left-1/2 -z-10 aspect-[1155/678] w-[36.125rem] max-w-none -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-40rem)] sm:w-[72.1875rem]"
                        style={{
                            clipPath:
                                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                        }}
                    />
                </div>
                <div className="mx-auto max-w-2xl text-center">
                    <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl">Biodata Konsultasi</h2>
                    <p className="mt-2 text-lg leading-8 text-gray-600">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia officiis fugit voluptatum..
                    </p>
                </div>
                {formOpen && (
                    <div>
                        <form onSubmit={handleSubmitNewKonsultasi} className="mx-auto mt-16 max-w-xl sm:mt-20">
                            <div className="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">
                                <div>
                                    <label htmlFor="first-name" className="block text-sm font-semibold leading-6 text-gray-900">
                                        First name
                                    </label>
                                    <div className="mt-2.5">
                                        <input
                                            value={newNama}
                                            onChange={e => setNewNama(e.target.value)}
                                            type="text"
                                            name="first-name"
                                            id="first-name"
                                            autoComplete="given-name"
                                            className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        />
                                    </div>
                                </div>
                                <div>
                                    <label htmlFor="last-name" className="block text-sm font-semibold leading-6 text-gray-900">
                                        Last name
                                    </label>
                                    <div className="mt-2.5">
                                        <input
                                            value={newNamaBelakang}
                                            onChange={e => setNewNamaBelakang(e.target.value)}
                                            type="text"
                                            name="last-name"
                                            id="last-name"
                                            autoComplete="family-name"
                                            className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        />
                                    </div>
                                </div>
                                <div className="sm:col-span-2">
                                    <label htmlFor="company" className="block text-sm font-semibold leading-6 text-gray-900">
                                        Umur
                                    </label>
                                    <div className="mt-2.5">
                                        <input
                                            value={newUmur}
                                            onChange={e => setNewUmur(e.target.value)}
                                            type="text"
                                            name="company"
                                            id="company"
                                            autoComplete="organization"
                                            className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        />
                                    </div>
                                </div>
                                <div className="sm:col-span-2">
                                    <label htmlFor="text" className="block text-sm font-semibold leading-6 text-gray-900">
                                        Alamat
                                    </label>
                                    <div className="mt-2.5">
                                        <input
                                            value={newAlamat}
                                            onChange={e => setNewAlamat(e.target.value)}
                                            type="text"
                                            name="text"
                                            id="text"
                                            autoComplete="text"
                                            className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        />
                                    </div>
                                </div>

                                <div className="sm:col-span-2">
                                    <label htmlFor="country" className="block text-sm font-semibold leading-6 text-gray-900">
                                        Jenis Kelamin
                                    </label>
                                    <div className="mt-2">
                                        <select
                                            value={newJenisKelamin}
                                            onChange={e => setNewJenisKelamin(e.target.value)}
                                            id="country"
                                            name="country"
                                            autoComplete="country-name"
                                            className="block w-full rounded-md border-0 px-3.5 py-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        >
                                            <option>---Plih---</option>
                                            <option>Laki-Laki</option>
                                            <option>Perempuan</option>

                                        </select>
                                    </div>
                                </div>


                                <div className="sm:col-span-2">
                                    <label htmlFor="phone-number" className="block text-sm font-semibold leading-6 text-gray-900">
                                        No Telepon
                                    </label>
                                    <div className="relative mt-2.5">
                                        <div className="absolute inset-y-0 left-0 flex items-center">
                                            <label htmlFor="country" className="sr-only">
                                                Country
                                            </label>
                                            <select
                                                id="country"
                                                name="country"
                                                className="h-full rounded-md border-0 bg-transparent bg-none py-0 pl-4 pr-9 text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm"
                                            >
                                                <option>US</option>
                                                <option>CA</option>
                                                <option>EU</option>
                                            </select>

                                        </div>
                                        <input
                                            value={newNoTelp}
                                            onChange={e => setNewNoTelp(e.target.value)}
                                            type="tel"
                                            name="phone-number"
                                            id="phone-number"
                                            autoComplete="tel"
                                            className="block w-full rounded-md border-0 px-3.5 py-2 pl-20 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="mt-10">
                                <button
                                    type="submit"
                                    className="block w-full rounded-md bg-indigo-600 px-3.5 py-2.5 text-center text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
                                >
                                    Send
                                </button>
                            </div>
                        </form>
                    </div>

                )}
                {!formOpen && (
                    <div>
                        <div className="bg-white p-8 mx-auto max-w-xl mt-8 rounded shadow">
                            <h2 className="text-xl font-semibold mb-4">Data Konsultasi Anda!</h2>
                            {/* Tampilkan detail hasil formulir di sini */}
                            <p>Nama: {newNama + ' ' + newNamaBelakang}</p>
                            <p>Umur: {newUmur}</p>
                            <p>Alamat: {newAlamat}</p>
                            <p>Jenis Kelamin: {newJenisKelamin}</p>
                            <p>No Telepon: {newNoTelp}</p>
                            {/* ... (tampilkan detail lainnya) */}
                        </div>
                        <form onSubmit={handleSubmit}>
                            <div className="bg-white p-8 mx-auto max-w-xl mt-8 rounded shadow">
                                <label>Pilih Yang Anda Rasakan:</label>
                                <ul className="list-none p-0">

                                    {/* {dataDariBackend.map((item, index) => (
                                        <li key={index} className="mb-4">
                                            {item.faktor_penyebab}:
                                            <select
                                                value={factorWeights[index] || ''}
                                                onChange={(e) => handleWeightChange(index, e.target.value)}
                                            >
                                                <option value="">Pilih Jawaban</option>
                                                <option value="0">Tidak</option>
                                                <option value="1">IYA</option>
                                            </select>
                                        </li>
                                    ))} */}

                                    {dataDariBackendPenyakit.map((penyakit, indexPenyakit) => (
                                        <div key={indexPenyakit}>
                                            <h3>{penyakit.nama_penyakit}</h3>
                                            <ul>
                                                {dataDariBackend.map((item, index) => (
                                                    <li key={index} className="mb-4">
                                                        {item.faktor_penyebab}:
                                                        <select
                                                        onChange={(event) => handleSelectChange(indexPenyakit, index, event)}
                                                        >
                                                            <option value="">Pilih Jawaban</option>
                                                            <option value="0">Tidak</option>
                                                            <option value="1">IYA</option>
                                                        </select>
                                                    </li>
                                                ))}
                                            </ul>
                                        </div>
                                    ))}

                                </ul>
                                <button className="bg-indigo-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2" type="submit">Diagnosa</button>
                            </div>

                        </form>
                    </div>
                )}
                <Modal modalOpen={modalOpen} setModalOpen={setModalOpen}>
                    {diagnosaResult && (
                        <div className="bg-white p-8 mx-auto max-w-xl mt-8 rounded shadow">
                            <h2 className="text-xl font-semibold mb-4">Hasil Diagnosa Anda Bapak/Ibu {newNama}</h2>
                            <div>
                                <h2>CBR Diagnosis:</h2>
                                <ul>
                                    {diagnosaResult.data.cbrDiagnosis.map((diagnosis, index) => (
                                        <li key={index}>
                                            <p>Penyakit: {diagnosis.penyakit}</p>
                                            <p>Hasil: {diagnosis.hasil?.toFixed(0)} %</p>
                                        </li>
                                    ))}
                                </ul>

                                <h2>CF Diagnosis:</h2>
                                <ul>
                                    {diagnosaResult.data.cfDiagnosis.map((diagnosis, index) => (
                                        <li key={index}>
                                            <p>Penyakit: {diagnosis.penyakit}</p>
                                            <p>CF: {diagnosis.CF?.toFixed(0)} %</p>
                                        </li>
                                    ))}
                                </ul>

                                <ul>
                                    {diagnosaResult && diagnosaResult.data && diagnosaResult.data.hasilAkhir && Array.isArray(diagnosaResult.data.hasilAkhir) && diagnosaResult.data.hasilAkhir.map((hasil, index) => (
                                        <li key={index}>
                                            <p>Nama Penyakit: {hasil.namaPenyakit}</p>
                                            <p>Detail Penyakit: {hasil.detailPenyakit}</p>
                                            <p>Solusi: {hasil.Solusi}</p>
                                        </li>
                                    ))}
                                </ul>

                            </div>
                            {/* <p>Solusi: {diagnosaResult.confidence}</p> */}
                            <button className="bg-indigo-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mr-2 mt-3">Simpan Hasil</button>
                            {/* ... (tampilkan detail lainnya) */}
                        </div>)}


                </Modal>
            </div>
        </div >
    )
}

export default AddKonsultasi