'use client'

import React, { FormEventHandler, useState } from 'react'
// import { IPenyakit } from '@/types/penyakit'
import { IFaktor } from '@/types/faktor'
import { AiFillEdit } from 'react-icons/ai'
import { BsFillTrash3Fill } from 'react-icons/bs'
import { useRouter } from 'next/navigation'
import Modal from './Modal'
// import { editPenyakit, deletePenyakit } from '@/api'
import { editFaktor,deleteFaktor } from '@/api'
import Swal from 'sweetalert2'

interface FaktorProps {
    faktor: IFaktor
}

const Penyakit: React.FC<FaktorProps> = ({ faktor }) => {
    const router = useRouter()
    const [openModalEdit, setOpenModalEdit] = useState<boolean>(false)
    const [openModalDelete, setOpenModalDelete] = useState<boolean>(false)
    // const [taskToEdit, setTaskToEdit] = useState(task.text)
    const [NamaFaktor, setNamaFaktor] = useState(faktor.faktor_penyebab);
    const [BobotFaktor, setBobotFaktor] = useState(faktor.bobot_faktor);
    // const [SolusiPenyakit, setSolusiPenyakit] = useState(penyakit.solusi_penyakit);

    const handleSubmitEditPenyakit: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        await editFaktor({
            id: faktor.id,
            faktor_penyebab: NamaFaktor,
            bobot_faktor: BobotFaktor
        })
        Swal.fire(
            'Success',
            'Success Edit Data',
            'success'
        )
        setNamaFaktor("")
        setBobotFaktor("")
        setOpenModalEdit(false)

        router.refresh()
    }

    const handleDeletePenyakit = async (id: string) => {
        await deleteFaktor(id);
        Swal.fire(
            'Success',
            'Success Delete Data',
            'success'
        )
        setOpenModalDelete(false)
        router.refresh()
    }
    return (
        <tr key={faktor.id}>
            <td>
                {faktor.faktor_penyebab}
            </td>
            <td>
                {faktor.bobot_faktor}
            </td>

            <td className='flex gap-5'>
                <AiFillEdit
                    onClick={() => setOpenModalEdit(true)}
                    cursor='pointer'
                    className="text-blue-500"
                    size={25} />

                <Modal modalOpen={openModalEdit} setModalOpen={setOpenModalEdit}>
                    <form onSubmit={handleSubmitEditPenyakit}>
                        <h3 className='font-bold text-lg'>Edit Faktor</h3>
                        <div className='modal-action display flex'>

                        </div>

                        <div className="sm:col-span-4 mt-2">
                            <div className="mt-2">
                                <input
                                    value={NamaFaktor}
                                    onChange={e => setNamaFaktor(e.target.value)}
                                    type="text"
                                    placeholder='type nama faktor'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div>
                        <div className="sm:col-span-4 mt-2">

                            <div className="mt-2">
                                <input
                                    value={BobotFaktor}
                                    onChange={e => setBobotFaktor(e.target.value)}
                                    type="text"
                                    placeholder='type Bobot Faktor'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div>

                        {/* <div className="sm:col-span-4 mt-2">

                            <div className="mt-2">
                                <input
                                    value={SolusiPenyakit}
                                    onChange={e => setSolusiPenyakit(e.target.value)}
                                    type="text"
                                    placeholder='type solusi penyakit'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div> */}

                        {/* <button type='submit' className='btn'>Submit</button> */}

                 
                    <button type='submit' className='btn mt-2'>Submit</button>
                </form>
            </Modal>

            <BsFillTrash3Fill
                onClick={() => setOpenModalDelete(true)}
                cursor='pointer'
                className="text-red-500"
                size={25} />
            <Modal modalOpen={openModalDelete} setModalOpen={setOpenModalDelete}>
                <h3>Are you sure,you want to delete this task?</h3>
                <div className='modal-action'>
                    <button
                        onClick={() => handleDeletePenyakit(faktor.id)}
                        className='btn'
                    >
                        Yes
                    </button>


                </div>
            </Modal>
        </td>

        </tr >
    )
}

export default Penyakit