
// import { IPenyakit } from '@/types/penyakit'
import { IFaktor } from '@/types/faktor';
import React from 'react'
// import Penyakit from './Faktor'
import Faktor from './Faktor'

interface FaktorProps{
    faktors:IFaktor[];
}
const FaktorList:React.FC<FaktorProps> = ({faktors}) => {
  return (
    <div className="overflow-x-auto">
    <table className="table">
        {/* head */}
        <thead>
            <tr>
                <th>Nama Faktor</th>
                <th>Bobot Faktor</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            {faktors.map(faktor =>(
                <Faktor key={faktor.id} faktor={faktor}/>
                // penyakit.detail_penyakit
            ))}
            {/* {faktors.map(penyakit=>(
            <Penyakit key={penyakit.id} penyakit={penyakit}>
            ))} */}
        </tbody>
    </table>
</div>
  )
}

export default FaktorList