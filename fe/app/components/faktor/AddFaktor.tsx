'use client'

import React, { FormEventHandler, useState } from 'react'
import Modal from './Modal'
// import { addPenyakit } from '@/api'
import { addFaktor } from '@/api'
import { IFaktor } from '@/types/faktor'

import { AiFillPlusCircle } from 'react-icons/ai'
import { useRouter } from 'next/navigation'
import Swal from 'sweetalert2'



const AddFaktor = () => {
    const router = useRouter();
    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [newNamaFaktor, setNewNamaFaktor] = useState('');
    const [newBobotFaktor, setNewBobotFaktor] = useState('');
    // const [newSolusiPenyakit, setNewSolusiPenyakit] = useState('');

    const handleSubmitNewTodo: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        await addFaktor({
            id: "1",
            faktor_penyebab: newNamaFaktor,
            bobot_faktor: newBobotFaktor
        })
        Swal.fire(
            'Success',
            'Success Add Data',
            'success'
          )
        setNewNamaFaktor("")
        setNewBobotFaktor("")
        // setNewDetailPenyakit("")
        setModalOpen(false)
        router.refresh()
    }

    return (
        <div>
            <button onClick={() => setModalOpen(true)} className='btn btn-primary w-full'>
                Add new Faktor<AiFillPlusCircle className='ml-2' size={18} /></button>
            <Modal modalOpen={modalOpen} setModalOpen={setModalOpen}>
                <form onSubmit={handleSubmitNewTodo}>
                    <h3 className='font-bold text-lg'>Add New Faktor</h3>
                    <div className='modal-action display flex'>

                    </div>
                    
                    <div className="sm:col-span-4 mt-2">
                        <div className="mt-2">
                            <input
                                value={newNamaFaktor}
                                onChange={e => setNewNamaFaktor(e.target.value)}
                                type="text"
                                placeholder='type nama faktor'
                                className='input input-bordered w-full'
                                required />
                        </div>
                    </div>
                    <div className="sm:col-span-4 mt-2">
                     
                        <div className="mt-2">
                            <input
                                value={newBobotFaktor}
                                onChange={e => setNewBobotFaktor(e.target.value)}
                                type="text"
                                placeholder='type bobot faktor'
                                className='input input-bordered w-full'
                                required />
                        </div>
                    </div>
                   
                    <button type='submit' className='btn mt-2'>Submit</button>

                </form>
            </Modal>
        </div>
    )
}

export default AddFaktor