"use client";
import Link from "next/link";
import React, { useEffect, useState } from "react";

const Header = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isHidden, setIsHidden] = useState(false);

  // Fungsi untuk memeriksa apakah access token masih valid
  const isAccessTokenValid = () => {
    const accessTokenExpiration = localStorage.getItem("accessTokenExpiration");
    // Periksa apakah accessTokenExpiration masih valid (misalnya, belum kedaluwarsa)
    return (
      accessTokenExpiration && Date.now() < parseInt(accessTokenExpiration, 10)
    );
  };

  // Fungsi untuk mendapatkan access token baru dari refresh token
  const getNewAccessToken = () => {
    // Lakukan permintaan ke server untuk mendapatkan access token baru menggunakan refresh token
    // Simpan access token baru dan waktu kedaluwarsanya di localStorage
    const newAccessToken = "access_token_baru"; // Gantilah dengan nilai access token baru dari server
    const accessTokenExpiration = Date.now() + 3600 * 1000; // Misalnya, kedaluwarsa dalam 1 jam
    localStorage.setItem("accessToken", newAccessToken);
    localStorage.setItem(
      "accessTokenExpiration",
      accessTokenExpiration.toString()
    );

    // Tandai pengguna sebagai login
    setIsLoggedIn(true);
  };

  useEffect(() => {
    // Cek apakah access token masih valid saat komponen dimuat
    if (isAccessTokenValid()) {
      // Jika access token masih valid, tandai pengguna sebagai login
      setIsLoggedIn(true);
      if (localStorage.getItem("role") === "ROLE_ADMIN") {
        setIsHidden(true);
      }
    } else {
      // Jika access token telah kedaluwarsa, dapatkan access token baru dari refresh token
      getNewAccessToken();
    }
  }, []); // [] sebagai parameter kedua useEffect agar efek ini hanya dijalankan sekali setelah komponen dimuat

  const handleLogout = () => {
    // Hapus informasi login dari localStorage saat logout
    localStorage.removeItem("accessToken");
    localStorage.removeItem("accessTokenExpiration");
    setIsLoggedIn(false);
  };

  return (
    <div className="Header bg-white  shadow">
      <div className="container mx-auto">
        <nav className="flex flex-wrap items-center justify-between p-4 ">
          <div className="lg:order-2 w-auto lg:w-1/5 lg:text-center">
            <a
              className="text-xl font-semibold font-heading"
              href="#"
              data-config-id="brand"
            >
              SPK Pernapasan
            </a>
          </div>
          <div className="block lg:hidden">
            <button className="navbar-burger flex items-center py-2 px-3 text-indigo-500 rounded border border-indigo-500">
              <svg
                className="fill-current h-3 w-3"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <title>Menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
              </svg>
            </button>
          </div>
          <div className="navbar-menu hidden lg:order-3 lg:block w-full lg:w-2/5 lg:text-right">
            <Link
              href="/"
              className="py-4 px-8 mr-6 leading-none text-indigo-700 bg-white font-semibold text-sm"
            >
              Home
            </Link>
            {isHidden ? (
              <React.Fragment>
                <Link
                  href="/penyakits"
                  className="py-4 px-8 mr-6 leading-none text-indigo-700 bg-white font-semibold text-sm lg:w-auto"
                >
                  Penyakit
                </Link>
                <Link
                  href="/pengetahuan"
                  className="py-4 px-8 mr-6 leading-none text-indigo-700 bg-white font-semibold text-sm lg:w-auto"
                >
                  Pengetahuan
                </Link>
                <Link
                  href="/faktor"
                  className="py-4 px-8 mr-6 leading-none text-indigo-700 bg-white font-semibold text-sm lg:w-auto"
                >
                  Faktor
                </Link>
              </React.Fragment>
            ) : (
              <React.Fragment>
              
              </React.Fragment>
            )}
            <Link
              href="/konsultasi"
              className="py-4 px-8 mr-6 leading-none text-indigo-700 bg-white font-semibold text-sm"
            >
              Konsultasi
            </Link>
            <Link
              href={isLoggedIn ? "/login" : "/"}
              className="bg-gray-800 hover:shadow-md text-white font-bold py-2 px-4 rounded active:bg-gray-700"
              onClick={isLoggedIn ? handleLogout : undefined}
            >
              {isLoggedIn ? "Logout" : "Login"}
            </Link>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Header;
