'use client'

import React, { FormEventHandler, useState } from 'react'
import Modal from './Modal'
import { addPenyakit } from '@/api'
import { IPenyakit } from '@/types/penyakit'

import { AiFillPlusCircle } from 'react-icons/ai'
import { useRouter } from 'next/navigation'
import Swal from 'sweetalert2'



const AddPenyakit = () => {
    const router = useRouter();
    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [newNamaPenyakit, setNewNamaPenyakit] = useState('');
    const [newDetailPenyakit, setNewDetailPenyakit] = useState('');
    const [newSolusiPenyakit, setNewSolusiPenyakit] = useState('');

    const handleSubmitNewTodo: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        await addPenyakit({
            id: "1",
            nama_penyakit: newNamaPenyakit,
            detail_penyakit: newDetailPenyakit,
            solusi_penyakit: newSolusiPenyakit
        })
        Swal.fire(
            'Success',
            'Success Add Data',
            'success'
          )
        setNewNamaPenyakit("")
        setNewSolusiPenyakit("")
        setNewDetailPenyakit("")
        setModalOpen(false)
        router.refresh()
    }

    return (
        <div>
            <button onClick={() => setModalOpen(true)} className='btn btn-primary w-full'>
                Add new Task<AiFillPlusCircle className='ml-2' size={18} /></button>
            <Modal modalOpen={modalOpen} setModalOpen={setModalOpen}>
                <form onSubmit={handleSubmitNewTodo}>
                    <h3 className='font-bold text-lg'>Add New Penyakit</h3>
                    <div className='modal-action display flex'>

                    </div>
                    
                    <div className="sm:col-span-4 mt-2">
                        <div className="mt-2">
                            <input
                                value={newNamaPenyakit}
                                onChange={e => setNewNamaPenyakit(e.target.value)}
                                type="text"
                                placeholder='type nama penyakit'
                                className='input input-bordered w-full'
                                required />
                        </div>
                    </div>
                    <div className="sm:col-span-4 mt-2">
                     
                        <div className="mt-2">
                            <input
                                value={newDetailPenyakit}
                                onChange={e => setNewDetailPenyakit(e.target.value)}
                                type="text"
                                placeholder='type detail penyakit'
                                className='input input-bordered w-full'
                                required />
                        </div>
                    </div>

                    <div className="sm:col-span-4 mt-2">
                 
                        <div className="mt-2">
                            <input
                                value={newSolusiPenyakit}
                                onChange={e => setNewSolusiPenyakit(e.target.value)}
                                type="text"
                                placeholder='type solusi penyakit'
                                className='input input-bordered w-full'
                                required />
                        </div>
                    </div>
                   
                    <button type='submit' className='btn mt-2'>Submit</button>

                </form>
            </Modal>
        </div>
    )
}

export default AddPenyakit