
import { IPenyakit } from '@/types/penyakit'
import React from 'react'
import Penyakit from './Penyakit'

interface PenyakitProps{
    penyakits:IPenyakit[];
}
const PenyakitList:React.FC<PenyakitProps> = ({penyakits}) => {
  return (
    <div className="overflow-x-auto">
    <table className="table">
        {/* head */}
        <thead>
            <tr>
                <th>Nama Penyakit</th>
                <th>Detail Penyakit</th>
                <th>Solusi Penyakit</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            {penyakits.map(penyakit =>(
                <Penyakit key={penyakit.id} penyakit={penyakit}/>
               
            ))}
        </tbody>
    </table>
</div>
  )
}

export default PenyakitList