'use client'

import React, { FormEventHandler, useState } from 'react'
import { IPenyakit } from '@/types/penyakit'
import { AiFillEdit } from 'react-icons/ai'
import { BsFillTrash3Fill } from 'react-icons/bs'
import { useRouter } from 'next/navigation'
import Modal from './Modal'
import { editPenyakit, deletePenyakit } from '@/api'
import Swal from 'sweetalert2'

interface PenyakitProps {
    penyakit: IPenyakit
}

const Penyakit: React.FC<PenyakitProps> = ({ penyakit }) => {
    const router = useRouter()
    const [openModalEdit, setOpenModalEdit] = useState<boolean>(false)
    const [openModalDelete, setOpenModalDelete] = useState<boolean>(false)
    const [NamaPenyakit, setNamaPenyakit] = useState(penyakit.nama_penyakit);
    const [DetailPenyakit, setDetailPenyakit] = useState(penyakit.detail_penyakit);
    const [SolusiPenyakit, setSolusiPenyakit] = useState(penyakit.solusi_penyakit);

    const handleSubmitEditPenyakit: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        await editPenyakit({
            id: penyakit.id,
            nama_penyakit: NamaPenyakit,
            detail_penyakit: DetailPenyakit,
            solusi_penyakit: SolusiPenyakit
        })
        Swal.fire(
            'Success',
            'Success Edit Data',
            'success'
        )
        setNamaPenyakit("")
        setDetailPenyakit("")
        setSolusiPenyakit("")
        setOpenModalEdit(false)

        router.refresh()
    }

    const handleDeletePenyakit = async (id: string) => {
        await deletePenyakit(id);
        Swal.fire(
            'Success',
            'Success Delete Data',
            'success'
        )
        setOpenModalDelete(false)
        router.refresh()
    }
    return (
        <tr key={penyakit.id}>
            <td>
                {penyakit.nama_penyakit}
            </td>
            <td>
                {penyakit.detail_penyakit}
            </td>
            <td>
                {penyakit.solusi_penyakit}
            </td>

            <td className='flex gap-5'>
                <AiFillEdit
                    onClick={() => setOpenModalEdit(true)}
                    cursor='pointer'
                    className="text-blue-500"
                    size={25} />

                <Modal modalOpen={openModalEdit} setModalOpen={setOpenModalEdit}>
                    <form onSubmit={handleSubmitEditPenyakit}>
                        <h3 className='font-bold text-lg'>Edit Penyakit</h3>
                        <div className='modal-action display flex'>

                        </div>

                        <div className="sm:col-span-4 mt-2">
                            <div className="mt-2">
                                <input
                                    value={NamaPenyakit}
                                    onChange={e => setNamaPenyakit(e.target.value)}
                                    type="text"
                                    placeholder='type nama penyakit'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div>
                        <div className="sm:col-span-4 mt-2">

                            <div className="mt-2">
                                <input
                                    value={DetailPenyakit}
                                    onChange={e => setDetailPenyakit(e.target.value)}
                                    type="text"
                                    placeholder='type detail penyakit'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div>

                        <div className="sm:col-span-4 mt-2">

                            <div className="mt-2">
                                <input
                                    value={SolusiPenyakit}
                                    onChange={e => setSolusiPenyakit(e.target.value)}
                                    type="text"
                                    placeholder='type solusi penyakit'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div>

                        {/* <button type='submit' className='btn'>Submit</button> */}

                 
                    <button type='submit' className='btn mt-2'>Submit</button>
                </form>
            </Modal>

            <BsFillTrash3Fill
                onClick={() => setOpenModalDelete(true)}
                cursor='pointer'
                className="text-red-500"
                size={25} />
            <Modal modalOpen={openModalDelete} setModalOpen={setOpenModalDelete}>
                <h3>Are you sure,you want to delete this task?</h3>
                <div className='modal-action'>
                    <button
                        onClick={() => handleDeletePenyakit(penyakit.id)}
                        className='btn'
                    >
                        Yes
                    </button>


                </div>
            </Modal>
        </td>

        </tr >
    )
}

export default Penyakit