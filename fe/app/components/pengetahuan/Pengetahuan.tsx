'use client'

import React, { FormEventHandler, useState } from 'react'
// import { IPenyakit } from '@/types/penyakit'
// import { IFaktor } from '@/types/faktor'

import { IPengetahuan } from '@/types/pengetahuan'
import { AiFillEdit } from 'react-icons/ai'
import { BsFillTrash3Fill } from 'react-icons/bs'
import { useRouter } from 'next/navigation'
import Modal from './Modal'
// import { editPenyakit, deletePenyakit } from '@/api'
// import { editFaktor,deleteFaktor } from '@/api'

import { editPengetahuan, deletePengetahuan } from '@/api'


import Swal from 'sweetalert2'

interface PengetahuanProps {
    pengetahuan: IPengetahuan
}

const Pengetahuan: React.FC<PengetahuanProps> = ({ pengetahuan }) => {
    const router = useRouter()
    const [openModalEdit, setOpenModalEdit] = useState<boolean>(false)
    const [openModalDelete, setOpenModalDelete] = useState<boolean>(false)
    const [newid_faktor, setid_faktor] = useState(pengetahuan.id_faktor);
    const [diagnosisId, setdiagnosisId] = useState(pengetahuan.diagnosisId);
    const [diagnosa, setdiagnosa] = useState(pengetahuan.diagnosa);
    const [BobotKepastian, setBobotKepastian] = useState(pengetahuan.BobotKepastian);

    const handleSubmitEditPenyakit: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();
        await editPengetahuan({
            id: pengetahuan.id,
            id_faktor: newid_faktor,
            diagnosisId:diagnosisId,
            diagnosa:diagnosa,
            BobotKepastian:BobotKepastian
        })
        Swal.fire(
            'Success',
            'Success Edit Data',
            'success'
        )
        setid_faktor("")
        setdiagnosisId("")
        setdiagnosa("")
        setBobotKepastian("")
        setOpenModalEdit(false)

        router.refresh()
    }

    const handleDeletePenyakit = async (id: string) => {
        await deletePengetahuan(id);
        Swal.fire(
            'Success',
            'Success Delete Data',
            'success'
        )
        setOpenModalDelete(false)
        router.refresh()
    }
    return (
        <tr key={pengetahuan.id}>
            <td>
                {pengetahuan.id_faktor}
            </td>
            <td>
                {pengetahuan.diagnosa}
            </td>

            <td>
                {pengetahuan.BobotKepastian}
            </td>

            <td className='flex gap-5'>
                <AiFillEdit
                    onClick={() => setOpenModalEdit(true)}
                    cursor='pointer'
                    className="text-blue-500"
                    size={25} />

                <Modal modalOpen={openModalEdit} setModalOpen={setOpenModalEdit}>
                    <form onSubmit={handleSubmitEditPenyakit}>
                        <h3 className='font-bold text-lg'>Edit Faktor</h3>
                        <div className='modal-action display flex'>

                        </div>

                        <div className="sm:col-span-4 mt-2">
                            <div className="mt-2">
                                <input
                                    value={newid_faktor}
                                    onChange={e => setid_faktor(e.target.value)}
                                    type="text"
                                    placeholder='type nama faktor'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div>

                        <div className="sm:col-span-4 mt-2">

                            <div className="mt-2">
                                <input
                                    value={diagnosa}
                                    onChange={e => setdiagnosa(e.target.value)}
                                    type="text"
                                    placeholder='type Bobot Faktor'
                                    className='input input-bordered w-full'
                                    required />
                            </div>
                        </div>

                        {/* <button type='submit' className='btn'>Submit</button> */}


                        <button type='submit' className='btn mt-2'>Submit</button>
                    </form>
                </Modal>

                <BsFillTrash3Fill
                    onClick={() => setOpenModalDelete(true)}
                    cursor='pointer'
                    className="text-red-500"
                    size={25} />
                <Modal modalOpen={openModalDelete} setModalOpen={setOpenModalDelete}>
                    <h3>Are you sure,you want to delete this task?</h3>
                    <div className='modal-action'>
                        <button
                            onClick={() => handleDeletePenyakit(pengetahuan.id)}
                            className='btn'
                        >
                            Yes
                        </button>


                    </div>
                </Modal>
            </td>

        </tr >
    )
}

export default Pengetahuan