import { IPengetahuan } from '@/types/pengetahuan';
import React from 'react'

import Pengetahuan from './Pengetahuan';

interface PengatahuanProps{
    pengetahuans:IPengetahuan[];
}
const PengetahuanList:React.FC<PengatahuanProps> = ({pengetahuans}) => {
  return (
    <div className="overflow-x-auto">
    <table className="table">
        {/* head */}
        <thead>
            <tr>
                <th>ID Faktor</th>
                <th>Diagnosa</th>
                <th>Bobot Kepastian</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            {pengetahuans.map(pengetahuan =>(
                <Pengetahuan key={pengetahuan.id} pengetahuan={pengetahuan}/>
                
            ))}
         
        </tbody>
    </table>
</div>
  )
}

export default PengetahuanList