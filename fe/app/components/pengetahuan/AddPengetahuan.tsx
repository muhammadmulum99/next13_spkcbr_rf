'use client'
import React, { FormEventHandler, useState, useEffect } from 'react';
import Modal from './Modal';
import { addPengetahuan } from '@/api';
import { IPengetahuan } from '@/types/pengetahuan';

import { AiFillPlusCircle } from 'react-icons/ai';
import { useRouter } from 'next/navigation'; // Menggunakan useRouter dari 'next/router'
import Swal from 'sweetalert2';
import axios from 'axios';

interface FaktorPenyebab {
    id: number;
    faktor_penyebab: string;
    bobot_faktor: string;
}

const AddPengetahuan = () => {
    const router = useRouter();
    const [modalOpen, setModalOpen] = useState<boolean>(false);
    const [dataDariBackend, setDataDariBackend] = useState<FaktorPenyebab[]>([]);
    const [selectedFactors, setSelectedFactors] = useState<string[]>([]);
    const [newdiagnosisId, setNewDiagnosisId] = useState<number | null>(null);
    const [newdiagnosisInput, setNewDiagnosisInput] = useState<string>('');
    const [penyakitOptions, setPenyakitOptions] = useState<{ label: string; value: string }[]>([]);
    const [faktorOptions, setFaktorOptions] = useState<{ label: string; value: string }[]>([]);

    useEffect(() => {
        // Mengambil data penyakit dari endpoint
        axios.get('http://localhost:8080/api/be/penyakit')
            .then(response => {
                const options = response.data.map((penyakit: any) => ({
                    label: penyakit.nama_penyakit,
                    value: penyakit.id.toString() // Konversi ID menjadi string
                }));
                setPenyakitOptions(options);
            })
            .catch(error => {
                console.error('Error:', error);
            });

        // Mengambil data faktor dari endpoint
        axios.get('http://localhost:8080/api/be/faktor')
            .then(response => {
                const options = response.data.map((faktor: any) => ({
                    label: faktor.faktor_penyebab,
                    value: faktor.id.toString() // Konversi ID menjadi string
                }));
                setDataDariBackend(response.data);
                setFaktorOptions(options);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }, []);

    const handleSubmitNewTodo: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();

        const newData = {
            id_faktorInput: selectedFactors.map(factor => parseInt(factor)),
            diagnosisIdInput: newdiagnosisId,
            diagnosisInput: newdiagnosisId !== null ? penyakitOptions.find(option => option.value === newdiagnosisId.toString())?.label || '' : '', // Mengambil label diagnosis berdasarkan ID
            bobotKepastianInput: selectedFactors.map(() => 0.5) // Dummy value for bobot, ganti dengan nilai yang sesuai
        };

        try {
            const response = await axios.post('http://localhost:8080/api/be/pengetahuan', newData);

            Swal.fire(
                'Success',
                'Success Add Data',
                'success'
            );

            setSelectedFactors([]);
            setNewDiagnosisId(null);
            setNewDiagnosisInput("");
            setModalOpen(false);
            router.refresh();
        } catch (error) {
            console.error('Error:', error);

            Swal.fire(
                'Error',
                'Failed to Add Data',
                'error'
            );
        }
    };


    return (
        <div>
            <button onClick={() => setModalOpen(true)} className='btn btn-primary w-full'>
                Add new Pengetahuan<AiFillPlusCircle className='ml-2' size={18} /></button>
            <Modal modalOpen={modalOpen} setModalOpen={setModalOpen}>
                <form onSubmit={handleSubmitNewTodo}>
                    <h3 className='font-bold text-lg'>Add New Pengetahuan</h3>
                    <div className='modal-action display flex'>
                    </div>

                    <div className="sm:col-span-4 mt-2">
                        <div className="mt-2">
                            <select
                                value={selectedFactors}
                                onChange={e => setSelectedFactors(Array.from(e.target.selectedOptions, option => option.value))}
                                className='w-full p-2 text-base border rounded focus:outline-none focus:border-blue-500'
                                multiple
                                required
                            >
                                {faktorOptions.map(option => (
                                    <option key={option.value} value={option.value}>
                                        {option.label}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <div className="sm:col-span-4 mt-2">
                        <div className="mt-2">
                            <select
                                value={newdiagnosisId !== null ? newdiagnosisId.toString() : ''}
                                onChange={e => setNewDiagnosisId(e.target.value !== '' ? parseInt(e.target.value) : null)}
                                className='w-full p-2 text-base border rounded focus:outline-none focus:border-blue-500'
                                required
                            >
                                <option value="">Select Diagnosis</option>
                                {penyakitOptions.map(option => (
                                    <option key={option.value} value={option.value}>
                                        {option.label}
                                    </option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <button type='submit' className='btn mt-2'>Submit</button>
                </form>
            </Modal>
        </div>
    )
}

export default AddPengetahuan;



