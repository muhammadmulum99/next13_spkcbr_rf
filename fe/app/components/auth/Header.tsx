'use client'

import Link from 'next/link';

interface HeaderProps {
    heading: string;
    paragraph: string;
    linkName: string;
    linkUrl?: string;
}

const Header: React.FC<HeaderProps> = ({ heading, paragraph, linkName, linkUrl = "#" }) => {
    return (
        <div className="mb-10">
            <div className="flex justify-center">
                <img
                    alt=""
                    className="h-14 w-14"
                    src="https://cdn-icons-png.flaticon.com/256/4534/4534941.png" />
            </div>
            <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
                {heading}
            </h2>
            <p className="mt-2 text-center text-sm text-gray-600 mt-5">
                {paragraph}{' '}
                <Link href={linkUrl} className="font-medium text-purple-600 hover:text-purple-500">
                    {/* <a className="font-medium text-purple-600 hover:text-purple-500"> */}
                        {linkName}
                    {/* </a> */}
                </Link>
            </p>
        </div>
    );
};

export default Header;
