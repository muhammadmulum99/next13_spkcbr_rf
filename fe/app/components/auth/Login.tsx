import React, { useState, ChangeEvent, FormEvent } from 'react';
import { loginFields } from "./constants/formFields";
// import { useNavigate } from 'react-router-dom';
import { useRouter } from 'next/navigation'
import FormAction from "./FormAction";
import FormExtra from "./FormExtra";
import Input from "./Input";

interface Field {
    id: string;
    labelText: string;
    labelFor: string;
    name: string;
    type: string;
    isRequired?: boolean;
    placeholder?: string;
}

const fields: Field[] = loginFields;
let fieldsState: { [key: string]: string } = {};
fields.forEach(field => fieldsState[field.id] = '');

const Login: React.FC = () => {
    const history = useRouter();
    const [loginState, setLoginState] = useState<{ [key: string]: string }>(fieldsState);

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setLoginState({ ...loginState, [e.target.id]: e.target.value });
        console.log({ ...loginState, [e.target.id]: e.target.value });
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        authenticateUser(loginState.username, loginState.password);
    }

    const authenticateUser = (username: string, password: string) => {
        const endpoint = `http://localhost:8080/api/auth/signin`;
        fetch(endpoint, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        })
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                throw new Error('Authentication failed');
            }
        })
        .then(data => {
            localStorage.setItem('accessToken', data.accessToken);
            localStorage.setItem('refreshToken', data.refreshToken);
            localStorage.setItem('role',data.roles)
            // history('/home');
            history.push('/')
        })
        .catch(error => {
            console.error('Error:', error.message);
            alert('Authentication failed: ' + error.message);
        });
    }

    return (
        <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
            <div className="-space-y-px">
                {fields.map(field =>
                    <Input
                        key={field.id}
                        handleChange={handleChange}
                        value={loginState[field.id]}
                        labelText={field.labelText}
                        labelFor={field.labelFor}
                        id={field.id}
                        name={field.name}
                        type={field.type}
                        isRequired={field.isRequired}
                        placeholder={field.placeholder}
                    />
                )}
            </div>

            <FormExtra />
            <FormAction handleSubmit={handleSubmit} text="Login" />
        </form>
    );
}

export default Login;
