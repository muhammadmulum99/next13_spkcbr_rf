const loginFields=[
    {
        labelText:"User Name",
        labelFor:"user-name",
        id:"username",
        name:"username",
        type:"text",
        autoComplete:"",
        isRequired:true,
        placeholder:"User Name",   
    },
    {
        labelText:"Password",
        labelFor:"password",
        id:"password",
        name:"password",
        type:"password",
        autoComplete:"current-password",
        isRequired:true,
        placeholder:"Password"   
    }
]

const signupFields=[
    {
        labelText:"Username",
        labelFor:"username",
        id:"username",
        name:"username",
        type:"text",
        autoComplete:"username",
        isRequired:true,
        placeholder:"Username"   
    },

    {
        labelText:"Role",
        labelFor:"Role",
        id:"role",
        name:"role",
        type:"text",
        autoComplete:"role",
        isRequired:true,
        placeholder:"Exp.. user or admin"   
    },
    {
        labelText:"Email address",
        labelFor:"emailaddress",
        id:"emailaddress",
        name:"email",
        type:"email",
        autoComplete:"email",
        isRequired:true,
        placeholder:"Email address"   
    },
    {
        labelText:"Password",
        labelFor:"password",
        id:"password",
        name:"password",
        type:"password",
        autoComplete:"current-password",
        isRequired:true,
        placeholder:"Password"   
    },
    {
        labelText:"Confirm Password",
        labelFor:"confirmpassword",
        id:"confirmpassword",
        name:"confirmpassword",
        type:"password",
        autoComplete:"confirm-password",
        isRequired:true,
        placeholder:"Confirm Password"   
    }
]

export {loginFields,signupFields}