'use client'
import React, { useState, ChangeEvent, FormEvent } from 'react';
import { signupFields } from "./constants/formFields";
import FormAction from "./FormAction";
import Input from "./Input";
import { useRouter } from 'next/navigation'

interface Field {
    id: string;
    labelText: string;
    labelFor: string;
    name: string;
    type: string;
    isRequired?: boolean;
    placeholder?: string;
}

const fields: Field[] = signupFields;
let fieldsState: { [key: string]: string } = {};
fields.forEach(field => fieldsState[field.id] = '');

const Signup: React.FC = () => {
    const history = useRouter();
    const [signupState, setSignupState] = useState<{ [key: string]: string }>(fieldsState);

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setSignupState({ ...signupState, [e.target.id]: e.target.value });
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (signupState.role !== "user" && signupState.role !== "admin") {
           alert("Role Harus Admin atau User")
        }


        if (signupState.password !== signupState.confirmpassword) {
            alert("Password dan Confirm Password Harus sama")
        }

        console.log(signupState);
        createAccount();
    }

    const createAccount = () => {
        //sabr
        const data = {
            username: signupState.username,
            email: signupState.emailaddress,
            password: signupState.password,
            roles: [signupState.role]
        };
    
        fetch('http://localhost:8080/api/auth/signup', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        })
        .then(response => {
            if (!response.ok) {
                throw new Error('Gagal Melakukan Register'+response);
       
            }
            return response.json();
          
        })
        .then(data => {
            console.log('Account created successfully:', data);

            alert(data)
            history.push('/login')
            // Lakukan apa yang perlu dilakukan setelah akun berhasil dibuat
        })
        .catch(error => {
            console.error('Gagal Melakukan Register:'+data, error);
            alert(error)
            // Lakukan penanganan kesalahan jika diperlukan
        });
    }

    return (
        <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
            <div className="">
                {fields.map(field =>
                    <Input
                        key={field.id}
                        handleChange={handleChange}
                        value={signupState[field.id]}
                        labelText={field.labelText}
                        labelFor={field.labelFor}
                        id={field.id}
                        name={field.name}
                        type={field.type}
                        isRequired={field.isRequired}
                        placeholder={field.placeholder}
                    />
                )}
                <FormAction handleSubmit={handleSubmit} text="Signup" />
            </div>
        </form>
    );
}

export default Signup;
