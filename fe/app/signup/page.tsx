import Header from "../components/auth/Header";
import Signup from "../components/auth/Signup";

export default function SignupPage() {
    return (
        <>
            <div className="flex items-center justify-center h-screen">
                <div className="bg-gray-200 p-8 rounded-lg shadow-md">
                    <Header
                        heading="Signup to create an account"
                        paragraph="Already have an account? "
                        linkName="Login"
                        linkUrl="/login"
                    />
                    <Signup />
                </div>
            </div>
        </>
    )
}