'use client'

import Header from "../components/auth/Header"
import Login from "../components/auth/Login"

export default function LoginPage() {
    return (
        <>
            <div className="flex items-center justify-center h-screen">
                <div className="bg-gray-200 p-8 rounded-lg shadow-md">
                    <Header
                        heading="Login to your account"
                        paragraph="Don't have an account yet? "
                        linkName="Signup"
                        linkUrl="/signup"
                    />
                    <Login />
                </div>
            </div>

        </>
    )
}