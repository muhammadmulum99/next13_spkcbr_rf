import React from 'react'
import { getAllPenyakit } from '@/api';

import PenyakitList from '../components/penyakit/PenyakitList';
import AddPenyakit from '../components/penyakit/AddPenyakit';
import Header from '../components/template/Header';

export default async function PenyakitPage() {
    const penyakits=await getAllPenyakit();
    console.log(penyakits);
  return (
    <div>
    <Header/>
    <main className="max-w-4xl mx-auto mt-4">
    <div className="text-center my-5 flex flex-col gap-4">
      <h1 className="text-2xl font-bold">List Penyakit</h1>
      <AddPenyakit/>
      
    </div>
    <PenyakitList penyakits={penyakits}/>
  </main>
  </div>
  )
}
