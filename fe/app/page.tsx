'use client'
// import { useRouter } from 'next/router';
import { useEffect } from 'react';

import { getAllTodos } from "@/api"
import AddTask from "./components/task/AddTask"
import TodoList from "./components/task/TodoList"
import Header from "./components/template/Header";
import { useRouter } from 'next/navigation';
import Link from "next/link";
import { BsJournalMedical } from "react-icons/bs";

import { AiOutlineInfoCircle } from "react-icons/ai";

import { BsPencilSquare } from "react-icons/bs";

export default function Home() {

  const router = useRouter();

  useEffect(() => {
    // Pengecekan access token dari localStorage
    const accessToken = localStorage.getItem('accessToken');

    // Jika access token tidak ada, arahkan pengguna ke halaman login
    if (!accessToken) {
      router.push('/login');
    }
  }, []); 
  // const tasks=await getAllTodos();
  // console.log(tasks)
  return (
    <div>
      <Header/>
      <div className="relative pt-16 pb-32 flex content-center items-center justify-center min-h-screen-75">
        <div
          className="absolute top-0 w-full h-full bg-center bg-cover "
          style={{
            backgroundImage:
              "url('https://kuyou.id/content/images/dr-meme-20200920070412.jpg')",
          }}
        >
          <span
            id="blackOverlay"
            className="w-full h-full absolute opacity-75 bg-black"
          ></span>
        </div>
        <div className="container relative mx-auto">
          <div className="items-center flex flex-wrap">
            <div className="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
              <div className="pr-12">
                <h1 className="text-white font-semibold text-5xl">
                  Konsultasikan Bersama Kami
                </h1>
                <p className="mt-4 text-lg text-white">
                  lorem ipsum dolor sit amet, consectet
                </p>
              </div>
            </div>
          </div>
        </div>
        <div
          className="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden h-70-px"
          style={{ transform: "translateZ(0)" }}
        >
          <svg
            className="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            version="1.1"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
          >
            <polygon
              className="text-blueGray-200 fill-current"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
      </div>

      <section className="pb-20 bg-blueGray-200 -mt-24">
        <div className="container mx-auto px-4">
          <div className="flex flex-wrap">
            <div className=" pt-6 w-full md:w-4/12 px-4 text-center">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                <div className="px-4 py-5 flex-auto">
                  <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400">
                    {/* <i className="fas fa-award"></i> */}
                    <AiOutlineInfoCircle size={25}/>
                  </div>
                  <h6 className="text-xl font-semibold">Tentang</h6>
                  <p className="mt-2 mb-4 text-blueGray-500">
                    Divide details about your product or agency work into parts.
                    A paragraph describing a feature will be enough.
                  </p>
                  <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                    {/* Tekan Disini */}
                    <Link href="/" className="text-black-200"> <small>Tekan Disini</small></Link>
                  </button>
                </div>
              </div>
            </div>

            <div className="pt-6 w-full md:w-4/12 px-4 text-center">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                <div className="px-4 py-5 flex-auto">
                  <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-slate-500">
                    {/* <i className="fas fa-retweet"></i> */}
                    <BsJournalMedical/>
                  </div>
                  <h6 className="text-xl font-semibold">Informasi Penyakit</h6>
                  <p className="mt-2 mb-4 text-blueGray-500">
                    Keep you user engaged by providing meaningful information.
                    Remember that by this time, the user is curious.
                  </p>
                  <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                  {/* <Link to="/konsultasi" className="text-blueGray-200"> */}
                  <Link href="/penyakits" className="text-black-200"> <small>Tekan Disini</small></Link>
                {/* </Link> */}
                    
                  </button>
                </div>
              </div>
            </div>

            <div className="pt-6 w-full md:w-4/12 px-4 text-center">
              <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg">
                <div className="px-4 py-5 flex-auto">
                  <div className="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-emerald-400">
                    <BsPencilSquare/>
                  </div>
                  <h6 className="text-xl font-semibold">Konsultasi</h6>
                  <p className="mt-2 mb-4 text-blueGray-500">
                    Write a few lines about each one. A paragraph describing a
                    feature will be enough. Keep you user engaged!
                  </p>
                  <button className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">
                  <Link href="/konsultasi" className="text-black-200"> <small>Tekan Disini</small></Link>
                  {/* <small>Tekan Disini</small> */}
                  {/* </Link> */}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

  )
}
