import React from 'react'
import { getAllFaktor } from '@/api';

import FaktorList from '../components/faktor/FaktorList';
import AddFaktor from '../components/faktor/AddFaktor';
import Header from '../components/template/Header';

export default async function FaktorPage() {
    const faktors=await getAllFaktor();
    console.log(faktors);
  return (
    <div>
    <Header/>
    <main className="max-w-4xl mx-auto mt-4">
    <div className="text-center my-5 flex flex-col gap-4">
      <h1 className="text-2xl font-bold">List Faktor</h1>
      <AddFaktor/>
      
    </div>
    <FaktorList faktors={faktors}/>
  </main>
  </div>
  )
}
