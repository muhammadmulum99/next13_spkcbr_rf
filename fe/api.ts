import { ITask } from "./types/task";
import { IPenyakit } from "./types/penyakit";
import { IFaktor } from "./types/faktor";
import { IPengetahuan } from "./types/pengetahuan";

const baseUrl='http://localhost:3001';
const baseUrlDev='http://localhost:8080'

export const getAllTodos =async ():Promise<ITask[]> => {
    const res=await fetch(`${baseUrl}/tasks`,{cache:'no-cache'});
    const todos=await res.json();
    return todos;
    
}
export const addTodo = async (todo:ITask):Promise<ITask>=>{
    const res=await fetch(`${baseUrl}/tasks`,{
        method: 'POST',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(todo),
    })
    const newTodo=await res.json();
    return newTodo;
}

export const editTodo = async (todo:ITask):Promise<ITask>=>{
    const res=await fetch(`${baseUrl}/tasks/${todo.id}`,{
        method: 'PUT',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(todo),
    })
    const updateTodo=await res.json();
    return updateTodo;
}

export const deleteTodo = async (id:string):Promise<void>=>{
    const res=await fetch(`${baseUrl}/tasks/${id}`,{
        method: 'DELETE',
    })
}


export const getAllPenyakit =async ():Promise<IPenyakit[]> => {
    const res=await fetch(`${baseUrlDev}/api/be/penyakit`,{cache:'no-cache'});
    const penyakit=await res.json();
    return penyakit;
    
}


export const addPenyakit = async (penyakit:IPenyakit):Promise<IPenyakit>=>{
    const res=await fetch(`${baseUrlDev}/api/be/penyakit`,{
        method: 'POST',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(penyakit),
    })
    const newPenyakit=await res.json();
    return newPenyakit;
}

export const editPenyakit = async (penyakit:IPenyakit):Promise<IPenyakit>=>{
    const res=await fetch(`${baseUrlDev}/api/be/penyakit/${penyakit.id}`,{
        method: 'PUT',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(penyakit),
    })
    const updatePenyakit=await res.json();
    return updatePenyakit;
}

export const deletePenyakit = async (id:string):Promise<void>=>{
    const res=await fetch(`${baseUrlDev}/api/be/penyakit/${id}`,{
        method: 'DELETE',
    })
}


export const getAllFaktor =async ():Promise<IFaktor[]> => {
    const res=await fetch(`${baseUrlDev}/api/be/faktor`,{cache:'no-cache'});
    const faktor=await res.json();
    return faktor;
    
}

export const addFaktor = async (faktor:IFaktor):Promise<IFaktor>=>{
    const res=await fetch(`${baseUrlDev}/api/be/faktor`,{
        method: 'POST',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(faktor),
    })
    const newFaktor=await res.json();
    return newFaktor;
}


export const editFaktor = async (faktor:IFaktor):Promise<IFaktor>=>{
    const res=await fetch(`${baseUrlDev}/api/be/faktor/${faktor.id}`,{
        method: 'PUT',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(faktor),
    })
    const updateFaktor=await res.json();
    return updateFaktor;
}

export const deleteFaktor = async (id:string):Promise<void>=>{
    const res=await fetch(`${baseUrlDev}/api/be/faktor/${id}`,{
        method: 'DELETE',
    })
}


export const getAllPengetahuan =async ():Promise<IPengetahuan[]> => {
    const res=await fetch(`${baseUrlDev}/api/be/pengetahuan`,{cache:'no-cache'});
    const pengetahuan=await res.json();
    return pengetahuan;
    
}

export const addPengetahuan = async (pengetahuan:IPengetahuan):Promise<IPengetahuan>=>{
    const res=await fetch(`${baseUrlDev}/api/be/pengetahuan`,{
        method: 'POST',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(pengetahuan),
    })
    const newPengetahuan=await res.json();
    return newPengetahuan;
}


export const editPengetahuan = async (pengetahuan:IPengetahuan):Promise<IPengetahuan>=>{
    const res=await fetch(`${baseUrlDev}/api/be/pengetahuan/${pengetahuan.id}`,{
        method: 'PUT',
        headers:{
            'content-type': 'application/json'
        },
        body: JSON.stringify(pengetahuan),
    })
    const updatePengetahuan=await res.json();
    return updatePengetahuan;
}

export const deletePengetahuan = async (id:string):Promise<void>=>{
    const res=await fetch(`${baseUrlDev}/api/be/pengetahuan/${id}`,{
        method: 'DELETE',
    })
}