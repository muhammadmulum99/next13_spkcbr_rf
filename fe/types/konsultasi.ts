export interface IKonsultasi{
    id:string;
    nama:string;
    umur:string;
    alamat:string;
    jenis_kelamin:string;
    no_telp:string;
}