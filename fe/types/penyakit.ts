export interface IPenyakit{
    id:string;
    nama_penyakit:string;
    detail_penyakit:string;
    solusi_penyakit:string;
}